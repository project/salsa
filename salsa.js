$(function() {
  /* Email field */
  $('#salsa-supporter-form #edit-Email').focus(function() {
    if ($(this).val() == Drupal.t('Enter your email for updates')) {
      $(this).val('');
    }  
  });
  $('#salsa-supporter-form #edit-Email').blur(function() {
    if ($(this).val() == '') {
      $(this).val(Drupal.t('Enter your email for updates'));
    }    
  });
});
