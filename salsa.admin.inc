<?php

/**
 * @file
 * Contains the administrative functions of the salsa module.
 *
 * This file is included by the salsa module, and includes the
 * settings form.
 */

/**
 * Menu callback for the Salsa module settings form.
 *
 * @ingroup forms 
 */
function salsa_settings() {
  $form['salsa_server'] = array(
    '#type' => 'select',
    '#title' => t('Salsa service'),
    '#default_value' => variable_get('salsa_server', 'http://salsa.democracyinaction.org'),
    '#options' => array(
      'http://sandbox.salsalabs.com' => t('Developer Sandbox'),
      'http://salsa.democracyinaction.org' => t('Democracy In Action'),
      'http://salsa.wiredforchange.com' => t('Wired for Change'),
    ),
    '#description' => t('Salsa Service to use (Democracy In Action, Wired for change or sandbox.)'),
    '#required' => TRUE,
  );
  $form['salsa_organization_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization key'),
    '#default_value' => variable_get('salsa_organization_key', ''),
    '#maxlength' => 10,
    '#description' => t('Organization key provided by Salsa (visible in the footer of the Salsa HQ page)'),
    '#required' => TRUE,
  );

  $form['salsa_api_auth'] = array(
    '#type' => 'fieldset',
    '#title' => t('API authentican credentials'),
    '#description' => t('Required for the use of API services'),
  );
  $form['salsa_api_auth']['salsa_api_email'] = array(
    '#type' => 'textfield', 
    '#title' => t('Campaign Manager username'),
    '#default_value' => variable_get('salsa_api_email', ''),
    '#maxlength' => 64,
  );
  $form['salsa_api_auth']['salsa_api_password'] = array(
    '#type' => 'textfield', 
    '#title' => t('Campaign Manager password'),
    '#default_value' => variable_get('salsa_api_password', ''),
    '#maxlength' => 64,
  );

  return system_settings_form($form);
}
